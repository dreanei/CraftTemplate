
PROJ_NAME=skycity
AD_IP=185.213.210.134
IP=185.213.210.134
PORT=30006
APP_ID='com.craft.skycity.exploration.sandbox.building.crafting.multiplayer.simulator.minigame'
APP_NAME='Craft Sky City'
#pls put all files in ./files folder, all images in png
icon=icon.png
background=background.png
splash=splash.png

function clear {
    rm -rf ./build/android/build/
}

function project_name {
    echo "changing config.h with "$PROJ_NAME".."
    PROJ_NAME="$(tr '[:upper:]' '[:lower:]' <<< ${PROJ_NAME:0:1})${PROJ_NAME:1}"
    sed -i -e 's|#define PROJECT_NAME ".*|#define PROJECT_NAME "'$PROJ_NAME'"|g' ./src/config.h
    PROJ_NAME="$(tr '[:lower:]' '[:upper:]' <<< ${PROJ_NAME:0:1})${PROJ_NAME:1}"
    sed -i -e 's|#define PROJECT_NAME_C.*|#define PROJECT_NAME_C "'$PROJ_NAME'"|g' ./src/config.h
    #sed -i -e 's|"image_button[4.2,2.8;3,1.5;//storage//emulated/0.*|"image_button[4.2,2.8;3,1.5;//storage//emulated//0//'PROJ_NAME_C'//textures//base//pack//button.png;btn_mp_connect;".. fgettext("Connect") .. "]" .. "'$PROJ_NAME'"|g' ./builtin/mainmenu/tab_simple_main.lua
    echo "changing CMakeLists.txt with "$PROJ_NAME".."
    sed -i -e 's|set(PROJECT_NAME_CAPITALIZED.*|set(PROJECT_NAME_CAPITALIZED "'$PROJ_NAME'")|g' ./CMakeLists.txt
}

function asset_folder_name {
    PROJ_NAME="$(tr '[:lower:]' '[:upper:]' <<< ${PROJ_NAME:0:1})${PROJ_NAME:1}"
    echo "changing MinetestAssetCopy.java with folder "$PROJ_NAME".."
    sed -i -e 's|File TempFolder = new File(baseDir.*/tmp/");|File TempFolder = new File(baseDir + "'$PROJ_NAME'/tmp/");|g' ./build/android/src/main/java/net/minetest/minetest/MinetestAssetCopy.java
    sed -i -e 's|OutputStream dst = new FileOutputStream(baseDir + ".*/.nomedia");|OutputStream dst = new FileOutputStream(baseDir + "'$PROJ_NAME'/.nomedia");|g' ./build/android/src/main/java/net/minetest/minetest/MinetestAssetCopy.java
}

function rename_assets {
    echo "renaming assets..."
    PROJ_NAME="$(tr '[:lower:]' '[:upper:]' <<< ${PROJ_NAME:0:1})${PROJ_NAME:1}"
    mv ./build/android/src/main/assets/Minetest ./build/android/src/main/assets/$PROJ_NAME
    sed -i -e 's|Minetest|'$PROJ_NAME'|g' ./build/android/src/main/assets/filelist.txt
    sed -i -e 's|Minetest|'$PROJ_NAME'|g' ./build/android/src/main/assets/index.txt
}

function clear_assets {
    echo "clearing old assets..."
    rm -rf ./build/android/src/main/assets/*
}

function api_ref {
    echo "setting hrefs..."
    PROJ_NAME="$(tr '[:upper:]' '[:lower:]' <<< ${PROJ_NAME:0:1})${PROJ_NAME:1}"
    sed -i -e 's|@GET(".*/ad-data.json")|@GET("'$PROJ_NAME'/ad-data.json")|g' ./build/android/src/main/java/net/minetest/minetest/data/AdMobData.kt
    sed -i -e 's|const val API_URL.*|const val API_URL: String = "http://'$AD_IP'/"|g' ./build/android/src/main/java/net/minetest/minetest/data/AdMobData.kt
}

function strings {
    echo "replacing string.xml stirngs..."
    #sed -i -e "s/APPNAME/$APP_NAME/g" ./build/android/src/main/res/values/strings.xml
    echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<resources>
	<string name=\"preparing_media\">Preparing media…</string>
	<string name=\"app_name\">$APP_NAME</string>
	<string name=\"need_permission\">Need this permission to store game\'s data. Pls enable it.</string>
	<string name=\"warn\">Warning!</string>
	<string name=\"title_activity_splash\">"$APP_NAME"</string>
	<string name=\"grant\">Grant permission</string>
	<string name=\"title_activity_spl\">SplActivity</string>
	<string name=\"splash_image\">splash_image</string>
	<string name=\"request_write\">App needs a permission to use storage</string>
</resources>" > ./build/android/src/main/res/values/strings.xml
}

function app_id {
    echo "setting app id..."
    sed -i -e 's|applicationId ".*|applicationId "'$APP_ID'"|g' ./build/android/build.gradle
}

function socket {
    echo "replacing ip's and ports..."
    sed -i -e 's|gamedata.address    = ".*|gamedata.address    = "'$IP'"|g' ./builtin/mainmenu/tab_simple_main.lua
    sed -i -e 's|gamedata.port	    = ".*|gamedata.port	    = "'$PORT'"|g' ./builtin/mainmenu/tab_simple_main.lua
    sed -i -e 's|core.settings:set("address", ".*|core.settings:set("address", "'$IP'")|g' ./builtin/mainmenu/tab_simple_main.lua
    sed -i -e 's|core.settings:set("remote_port", ".*|core.settings:set("remote_port", "'$PORT'")|g' ./builtin/mainmenu/tab_simple_main.lua
}

function files {
    echo "copy icon and background..."
    cp -f ./files/$icon ./build/android/src/main/res/drawable-hdpi/icon.png
    cp -f ./files/$splash ./build/android/src/main/res/drawable-hdpi/splash.png
    cp -f ./files/$background ./textures/base/pack/background.png
    cp -f ./files/$splash ./textures/base/pack/splash.png
}

function add_ssl_libs {
    cp -f ./files/libssl.so ./build/android/libs/armeabi-v7a/libssl.so
    cp -f ./files/libcrypto.so ./build/android/libs/armeabi-v7a/libcrypto.so
}

set -e
#Executing functions
clear
project_name
asset_folder_name
socket
files
strings
app_id
clear_assets
api_ref

#Executing makefile
cd ./build/android
echo "ndk.dir = /home/dreanei/ndk11/
sdk.dir = /home/dreanei/android-sdk-linux/" > ./local.properties
make -f Makefile
cd ../..
#end executing makefile

rename_assets
add_ssl_libs

