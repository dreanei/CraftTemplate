package net.minetest.minetest;

import android.app.NativeActivity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import net.minetest.minetest.data.AdMobAPI;
import net.minetest.minetest.data.entity.AdData;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MtNativeActivity extends NativeActivity {

	private AdData adData = new AdData();
	private InterstitialAd mInterstitialAd;
	private CompositeDisposable disposables;
	private AdListener mInterstitialAdListener = new AdListener() {
		@Override
		public void onAdClosed() {
			loadInterstitial();
		}
	};


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		m_MessagReturnCode = -1;
		m_MessageReturnValue = "";
        adData.setStartDelay(300);
        adData.setDelay(300);
        adData.setTimeUnit("SECONDS");
        disposables = new CompositeDisposable();
        disposables.add(((App) getApplication()).getInitCompletable().subscribe(new Action() {
            @Override
            public void run() {
                initAd(new AdMobAPI());
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) {
                initAd(new AdMobAPI());
            }
        }));
	}


	void initInterstitial() {
		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId(adData.getInterstitialId());
		mInterstitialAd.setAdListener(mInterstitialAdListener);
	}

	void loadInterstitial() {
		mInterstitialAd.loadAd(new AdRequest.Builder().build());
	}

	void showInterstitial() {
		if (mInterstitialAd.isLoaded()) {
			mInterstitialAd.show();
		}
	}

	private void initAd(AdMobAPI api) {
		disposables.add(api.getAdDataObservable()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.take(1).subscribe(new Consumer<AdData>() {
					@Override
					public void accept(AdData data) {
						adData = data;
						initInterstitial();
						loadInterstitial();
						runAdTimer();
					}
				}, new Consumer<Throwable>() {
					@Override
					public void accept(Throwable t) {
						t.printStackTrace();
					}
				}));
	}

	private void runAdTimer() {
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(new Runnable() {
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						showInterstitial();
					}
				});
			}
			// }, adData.getStartDelay(), adData.getDelay(), TimeUnit.valueOf(adData.getTimeUnit()));
		}, adData.getStartDelay(), adData.getDelay(), TimeUnit.valueOf(adData.getTimeUnit()));
	}

	@Override
	public void onDestroy() {
		disposables.dispose();
		super.onDestroy();
	}

	public void copyAssets() {
		Intent intent = new Intent(this, MinetestAssetCopy.class);
		startActivity(intent);
	}

	public void showDialog(String acceptButton, String hint, String current,
			int editType) {
		Intent intent = new Intent(this, MinetestTextEntry.class);
		Bundle params = new Bundle();
		params.putString("acceptButton", acceptButton);
		params.putString("hint", hint);
		params.putString("current", current);
		params.putInt("editType", editType);
		intent.putExtras(params);
		startActivityForResult(intent, 101);
		m_MessageReturnValue = "";
		m_MessagReturnCode   = -1;
	}

	public static native void putMessageBoxResult(String text);

	/* ugly code to workaround putMessageBoxResult not beeing found */
	public int getDialogState() {
		return m_MessagReturnCode;
	}

	public String getDialogValue() {
		m_MessagReturnCode = -1;
		return m_MessageReturnValue;
	}

	public float getDensity() {
		return getResources().getDisplayMetrics().density;
	}

	public int getDisplayWidth() {
		return getResources().getDisplayMetrics().widthPixels;
	}

	public int getDisplayHeight() {
		return getResources().getDisplayMetrics().heightPixels;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent data) {
		if (requestCode == 101) {
			if (resultCode == RESULT_OK) {
				String text = data.getStringExtra("text");
				m_MessagReturnCode = 0;
				m_MessageReturnValue = text;
			}
			else {
				m_MessagReturnCode = 1;
			}
		}
	}

	static {
		System.loadLibrary("openal");
		System.loadLibrary("ogg");
		System.loadLibrary("vorbis");
		System.loadLibrary("ssl");
		System.loadLibrary("crypto");
		System.loadLibrary("gmp");
		System.loadLibrary("iconv");
		System.loadLibrary("minetest");
	}

	private int m_MessagReturnCode;
	private String m_MessageReturnValue;
}
