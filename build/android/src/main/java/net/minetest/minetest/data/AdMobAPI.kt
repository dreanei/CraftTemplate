package net.minetest.minetest.data


import android.util.Log
import io.reactivex.schedulers.Schedulers
import net.minetest.minetest.data.entity.AdData
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

data class AdMobAPI(var adData: AdData = AdData()) {

    private val api: API = Retrofit.Builder()
            .baseUrl(AdMobData.API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(API::class.java)

    val adDataObservable
        get() = api.getAdsData()

    init {
        api.getAdsData().subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(
                { adData = it },
                {
                    Log.e("load ads error", it.localizedMessage)
                    it.printStackTrace()
                }
        )
    }

}