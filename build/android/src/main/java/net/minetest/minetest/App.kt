package net.minetest.minetest


import android.app.Application
import net.minetest.minetest.data.AdMobAPI
import net.minetest.minetest.data.entity.AdData
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class App : Application() {

    val api: AdMobAPI = AdMobAPI()
    private var adData: AdData = AdData()

    private var requestin: Boolean = false

    var exit: Boolean = false

    fun request() {
        requestin = true
    }

    val initCompletable: Completable = Completable.create { emitter ->
        api.adDataObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .take(1)
                .subscribe({ data ->
                    adData = data
                    val configBuilder = YandexMetricaConfig.newConfigBuilder(adData.appMetricaKey)
                    YandexMetrica.activate(applicationContext, configBuilder.build())
                    YandexMetrica.enableActivityAutoTracking(this)
                    emitter.onComplete()
                }, { t ->
                    emitter.onError(t)
                })
    }

}