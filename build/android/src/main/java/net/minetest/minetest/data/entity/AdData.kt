package net.minetest.minetest.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import net.minetest.minetest.data.AdMobData
import java.util.concurrent.TimeUnit


data class AdData(
        @SerializedName("startDelay")
        @Expose
        var startDelay: Int = 300,

        @SerializedName("delay")
        @Expose
        var delay: Int = 600,

        @SerializedName("timeUnit")
        @Expose
        var timeUnit: String = TimeUnit.SECONDS.name,

        @SerializedName("appId")
        @Expose
        var appId: String = AdMobData.APP_ID_DEBUG,

        @SerializedName("bannerId")
        @Expose
        var bannerId: String = AdMobData.TOP_BANNER_ID_DEBUG,

        @SerializedName("interstitialId")
        @Expose
        var interstitialId: String = AdMobData.TIMEOUT_AD_ID_DEBUG,

        @SerializedName("appMetricaKey")
        @Expose
        var appMetricaKey: String = ""
)