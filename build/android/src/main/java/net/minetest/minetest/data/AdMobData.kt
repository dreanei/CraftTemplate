package net.minetest.minetest.data

object AdMobData {
    const val APP_ID_DEBUG: String = "ca-app-pub-3940256099942544~3347511713"
    const val TOP_BANNER_ID_DEBUG: String = "ca-app-pub-3940256099942544/6300978111"
    const val TIMEOUT_AD_ID_DEBUG: String = "ca-app-pub-3940256099942544/1033173712"

    const val API_URL: String = "http://185.213.210.134/"
}