package net.minetest.minetest.data

import io.reactivex.Observable
import net.minetest.minetest.data.entity.AdData
import retrofit2.http.GET

interface API {
    @GET("skycity/ad-data.json")
    fun getAdsData(): Observable<AdData>
}
