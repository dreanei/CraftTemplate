package net.minetest.minetest

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_splash.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions


class SplashActivity : Activity() {

    companion object {
        private const val RC_WRITE_RAD_EXTERNAL_STORAGE = 111
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        if (isAlreadyHavePermissions())
            startActivity(Intent(this@SplashActivity, MtNativeActivity::class.java))
        else continueStuff()
        splash_img.setOnClickListener { continueStuff() }
    }

    private fun isAlreadyHavePermissions(): Boolean =
            EasyPermissions.hasPermissions(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                    EasyPermissions.hasPermissions(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>?, grantResults: IntArray?) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions!!, grantResults!!, this)
    }

    @AfterPermissionGranted(RC_WRITE_RAD_EXTERNAL_STORAGE)
    private fun continueStuff() {
        if (isAlreadyHavePermissions()) {
            startActivity(Intent(this@SplashActivity, net.minetest.minetest.MtNativeActivity::class.java))
        } else {
            EasyPermissions.requestPermissions(this@SplashActivity, getString(R.string.request_write),
                    RC_WRITE_RAD_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }
}
